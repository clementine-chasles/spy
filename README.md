# Application de gestion d'espions #

### Consigne ###

Enrichir une API RESTful de gestion d'espions.

L'objectif est de répondre au mieux au cahier des charges demandé. La présence de tests et de documentation de cette API est laissé à votre appréciation.

En cas de manque dans le cahier des charges, les décisions sont laissées à votre appréciation en les justifiant.

Faire un fork du repository.

Plateforme : Java 8 + MySQL + Linux + Tomcat

### Exercice ###

Pour assurer le suivi des missions, une agence a besoin d'une application pour gérer les affectations de ses espions.

Voici le cahier des charges transmis par l'agence :

### Cahier des charges ###

### Espion ###

* Un espion sait parler une ou plusieurs langues (et peut les renseigner dans l'app).
* Un espion peut choisir une mission.
* Une mission nécessite de connaitre un ensemble de langues.
* Un espion ne peut pas choisir une mission dont il ne maitrise pas l'ensemble de langues.
* Un espion peut abandonner une mission.
* Un espion ne peut pas avoir plus d'une mission à la fois.
* Une mission peut être choisie par plusieurs espions.
* Un espion peut compléter sa mission actuelle.
* Un espion peut consulter son historique de mission.

### Directeur ###

* Un directeur peut consulter les missions en cours.
* Un directeur peut créer une mission.
* Un directeur peut voir l'historique de toutes les missions.

### Sécurité (optionnelle) ###

* Un espion peut s'authentifier avec un identifiant et un mot de passe.
* Un utilisateur non authentifié ne peut pas accéder à la plateforme.
* Un directeur possède une authentification spéciale.
* Un directeur peut inscrire un nouvel espion.
* Un directeur peut désactiver le compte d'un espion.

***

# Notes #

À cause de la limitation en temps j'ai choisi d'implémenter :

### La gestion des langues ###

* Le code repose sur une table de référence : language
* Il n'y a qu'un service en lecture, permettant de récupérer le code pour ajouter une langue à un espion
* Les langues étant nécessaires aux missions et aux espions, j'ai voulu l'implémenter rapidement

### La création d'un espion avec ses langues connues ###

* Si le code n'existe pas, le serveur renvoie une erreur HTTP 400
* La création d'un espion existait déjà, j'ai simplement ajouté la sauvegarde des infos sur les langues connues

### Une partie du modèle de données ###

* J'ai été un peu optimiste sur le temps qu'il me faudrait pour implémenter les services que j'avais en tête, la majeure partie du modèle n'est pas testé

### Un test unitaire sur la classe Spy ###

* Comme exemple de la stratégie de tests : dans un premier temps je voulais simplement implémenter les tests du package model, car ce sont les classes qui allaient contenir la logique métier

### Pas de javadoc ###

* La documentation de l'API en ligne est séparée de la javadoc. Le code n'étant pas destiné à être packagé comme librairie, j'ai choisi de ne pas alourdir le code avec de la javadoc
* Je suis partie du principe que les conventions de nommage des classes/méthodes/attributs étaient assez claires pour ne pas avoir besoin de générer de javadoc inutile

### Ce que je voulais implémenter ###

* Ajouter des classes représentant les objets renvoyés par l'API pour ajouter une couche d'abstraction entre la signature de l'API et celle du modèle