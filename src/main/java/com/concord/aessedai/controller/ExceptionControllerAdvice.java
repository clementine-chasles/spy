package com.concord.aessedai.controller;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.concord.aessedai.exception.BadRequestException;
import com.concord.aessedai.exception.ResourceNotFound;

@ControllerAdvice
public class ExceptionControllerAdvice {

	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	@ResponseBody
	@ExceptionHandler(value = Exception.class)
	public Object unexpectedException(final Exception e) {
		final Map<String, Object> result = new HashMap<>();
		result.put("type", "UNEXPECTED");

		final Map<String, Object> details = new HashMap<>();
		details.put("message", e.getMessage());
		details.put("class", e.getClass().getName());
		details.put("stack", e.getStackTrace());
		result.put("details", details);

		return result;
	}

	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	@ResponseBody
	@ExceptionHandler(value = ResourceNotFound.class)
	public Object unexpectedException(final ResourceNotFound e) {
		final Map<String, Object> result = new HashMap<>();
		result.put("type", "RESOURCE_NOT_FOUND");

		final Map<String, Object> details = new HashMap<>();
		details.put("resource", e.getResource());
		details.put("id", e.getId());
		result.put("details", details);

		return result;
	}

	@ResponseStatus(value = HttpStatus.BAD_REQUEST)
	@ResponseBody
	@ExceptionHandler(value = BadRequestException.class)
	public Object unexpectedException(final BadRequestException e) {
		final Map<String, Object> result = new HashMap<>();
		result.put("type", "BAD_REQUEST");

		final Map<String, Object> details = new HashMap<>();
		details.put("resource", e.getResource());
		details.put("type", e.getType());
		result.put("details", details);

		return result;
	}
}
