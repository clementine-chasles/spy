package com.concord.aessedai.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/healthcheck")
public class HealthCheckController {

	@RequestMapping
	@ResponseStatus(value = HttpStatus.OK)
	public String healthCheck() {
		return "ok";
	}
}
