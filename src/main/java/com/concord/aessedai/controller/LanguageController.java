package com.concord.aessedai.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.concord.aessedai.model.Language;
import com.concord.aessedai.service.api.LanguageService;

@RestController
@RequestMapping(value = "/language")
public class LanguageController {

	@Autowired
	private LanguageService languageService;
	

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public List<Language> get() {
		return languageService.getAll();
	}

}
