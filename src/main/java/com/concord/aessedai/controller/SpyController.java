package com.concord.aessedai.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.concord.aessedai.model.Spy;
import com.concord.aessedai.service.api.SpyService;

@RestController
@RequestMapping(value = "/spy")
public class SpyController {

	@Autowired
	private SpyService spyService;

	@RequestMapping(method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.CREATED)
	@ResponseBody
	public Spy create(@RequestBody final SpyCreateCmd cmd) {
		return spyService.create(cmd);
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void delete(@PathVariable(value = "id") Long id) {
		spyService.delete(id);
	}

	@RequestMapping(method = RequestMethod.PATCH, value = "/{id}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void update(@PathVariable(value = "id") final Long id,
	                   @RequestBody final SpyUpdateCmd cmd) {
		spyService.update(id, cmd.getName());
	}
}
