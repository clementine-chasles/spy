package com.concord.aessedai.controller;

import java.io.Serializable;
import java.util.List;

public class SpyCreateCmd implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String name;
	private List<String> languages;
	
	public SpyCreateCmd() {}
	
	public SpyCreateCmd(String name, List<String> languages) {
		this.name = name;
		this.languages = languages;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
	
	public List<String> getLanguages() {
		return languages;
	}
	
	public void setLanguages(List<String> languages) {
		this.languages = languages;
	}

}
