package com.concord.aessedai.dao.api;

import java.util.List;

import com.concord.aessedai.model.Language;

public interface LanguageDAO {

	Language getById(Long id);
	Language getByCode(String code);
	List<Language> getAll();

}
