package com.concord.aessedai.dao.api;

import com.concord.aessedai.model.Spy;

public interface SpyDAO {

	Long save(Spy spy);

	Spy getById(Long id);

	void delete(Spy spy);

	void update(Spy spy);
}
