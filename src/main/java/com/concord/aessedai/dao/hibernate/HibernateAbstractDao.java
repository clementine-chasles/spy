package com.concord.aessedai.dao.hibernate;

import java.io.Serializable;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public abstract class HibernateAbstractDao<E, I extends Serializable> {

	@Autowired
	protected SessionFactory sessionFactory;

	private final Class<E> klazz;

	public HibernateAbstractDao(final Class<E> klazz) {
		this.klazz = klazz;
		this.klazz.getName();
	}

	public I save(final E entity) {
		return (I) getCurrentSession().save(entity);
	}

	public void update(final E entity) {
		getCurrentSession().update(entity);
	}

	public E getById(final I id) {
		return (E) getCurrentSession().get(klazz, id);
	}

	public void delete(final E entity) {
		getCurrentSession().delete(entity);
	}

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
}
