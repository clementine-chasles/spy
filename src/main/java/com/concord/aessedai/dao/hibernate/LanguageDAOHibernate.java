package com.concord.aessedai.dao.hibernate;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.concord.aessedai.dao.api.LanguageDAO;
import com.concord.aessedai.exception.BadRequestException;
import com.concord.aessedai.model.Language;

@Repository
public class LanguageDAOHibernate extends HibernateAbstractDao<Language, Long> implements LanguageDAO {

	public LanguageDAOHibernate() {
		super(Language.class);
	}

	@Override
	public Language getByCode(String code) {
		Criteria criteria = getCurrentSession().createCriteria(Language.class);
		Object res = criteria.add(Restrictions.eq("code", code))
                .uniqueResult();
		if(res == null) {
			throw new BadRequestException(code, "language");
		}
		return (Language)res;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Language> getAll() {
		return getCurrentSession().createCriteria(Language.class).list();
	}

}
