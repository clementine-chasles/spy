package com.concord.aessedai.dao.hibernate;

import org.springframework.stereotype.Repository;

import com.concord.aessedai.dao.api.SpyDAO;
import com.concord.aessedai.model.Spy;

@Repository
public class SpyDAOHibernate extends HibernateAbstractDao<Spy, Long> implements SpyDAO {

	public SpyDAOHibernate() {
		super(Spy.class);
	}

}
