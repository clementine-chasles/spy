package com.concord.aessedai.exception;

public class BadRequestException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private final String resource;
	private final String type;

	public BadRequestException(String resource, String type) {
		super("bad request " + resource + " of type " + type);
		this.resource = resource;
		this.type = type;
	}
	
	public String getResource() {
		return resource;
	}

	public String getType() {
		return type;
	}

}
