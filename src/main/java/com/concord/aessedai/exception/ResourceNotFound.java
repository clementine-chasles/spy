package com.concord.aessedai.exception;

public class ResourceNotFound extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private final String resource;
	private final Long id;

	public ResourceNotFound(final String resource, final Long id) {
		super("Resource not found: " + resource + "[" + id + "]");
		this.resource = resource;
		this.id = id;
	}

	public String getResource() {
		return resource;
	}

	public Long getId() {
		return id;
	}

}
