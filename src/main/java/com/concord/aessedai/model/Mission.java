package com.concord.aessedai.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "mission")
public class Mission {

	@Id
	@GeneratedValue
	@Column(name = "mission_id")
	private Long id;
	
	@ManyToMany
	@JoinTable(name = "mission_language", joinColumns = @JoinColumn(name = "mission_id"), inverseJoinColumns = @JoinColumn(name = "lang_id"))
	private List<Language> languages;
	
    @OneToMany(cascade=CascadeType.ALL, mappedBy = "ongoing")
	private List<Spy> spies;
	
	@OneToMany(mappedBy = "mission")
	private List<MissionSpyHistory> history;
    
    public Mission() {}

}
