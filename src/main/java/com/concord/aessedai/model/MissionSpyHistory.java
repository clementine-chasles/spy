package com.concord.aessedai.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "mission_history")
public class MissionSpyHistory {

	@Id
	@GeneratedValue
	@Column(name = "mission_history_id")
	private Long id;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "spy_id")
	private Spy spy;
	
	@ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "mission_id")
	private Mission mission;
	
	@Enumerated(EnumType.STRING)
	private SpyMissionStatus status;
	
	public MissionSpyHistory() {}

}
