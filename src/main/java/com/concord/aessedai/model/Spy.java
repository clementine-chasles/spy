package com.concord.aessedai.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "spy")
public class Spy {

	@Id
	@GeneratedValue
	@Column(name = "spy_id")
	private Long id;

	@Column(name = "name")
	private String name;
	
	@ManyToMany
	@JoinTable(name = "spy_language", joinColumns = @JoinColumn(name = "spy_id"), inverseJoinColumns = @JoinColumn(name = "lang_id"))
	private List<Language> languages;
	
	@ManyToOne(cascade=CascadeType.ALL)
	private Mission ongoing;
	
	@OneToMany(mappedBy = "spy")
	private List<MissionSpyHistory> history;

	public Spy() {
		// Empty constructor for hibernate
	}

	public Long getId() {
		return id;
	}

	public Spy(final String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}
	
	public List<Language> getLanguages() {
		if(languages == null) {
			languages = new ArrayList<Language>();
		}
		return languages;
	}
	
	public void learnLanguage(Language lang) {
		getLanguages().add(lang);
	}

}
