package com.concord.aessedai.service.api;

import java.util.List;

import com.concord.aessedai.model.Language;

public interface LanguageService {
	
	List<Language> getAll();

}
