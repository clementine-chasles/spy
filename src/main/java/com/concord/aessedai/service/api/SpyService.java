package com.concord.aessedai.service.api;

import com.concord.aessedai.controller.SpyCreateCmd;
import com.concord.aessedai.model.Spy;

public interface SpyService {

	Spy create(SpyCreateCmd spyToCreate);

	void delete(Long id);

	void update(Long id, String name);
}
