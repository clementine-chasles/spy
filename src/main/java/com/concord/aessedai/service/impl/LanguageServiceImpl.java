package com.concord.aessedai.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.concord.aessedai.dao.api.LanguageDAO;
import com.concord.aessedai.model.Language;
import com.concord.aessedai.service.api.LanguageService;

@Service
@Transactional(readOnly = true)
public class LanguageServiceImpl implements LanguageService {
	
	@Autowired
	private LanguageDAO languageDAO;

	@Override
	public List<Language> getAll() {
		return languageDAO.getAll();
	}

}
