package com.concord.aessedai.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.concord.aessedai.controller.SpyCreateCmd;
import com.concord.aessedai.dao.api.LanguageDAO;
import com.concord.aessedai.dao.api.SpyDAO;
import com.concord.aessedai.exception.ResourceNotFound;
import com.concord.aessedai.model.Spy;
import com.concord.aessedai.service.api.SpyService;

@Service
@Transactional(readOnly = true)
public class SpyServiceImpl implements SpyService {

	@Autowired
	private SpyDAO spyDao;
	
	@Autowired
	private LanguageDAO languageDAO;

	@Override
	@Transactional(readOnly = false)
	public Spy create(final SpyCreateCmd spyToCreate) {
		final Spy spy = new Spy(spyToCreate.getName());
		for(String lang : spyToCreate.getLanguages()) {
			spy.learnLanguage(languageDAO.getByCode(lang));
		}
		spyDao.save(spy);
		return spy;
	}

	@Override
	@Transactional(readOnly = false)
	public void delete(final Long id) {
		final Spy spy = spyDao.getById(id);

		if (spy == null) {
			throw new ResourceNotFound("spy", id);
		}
		spyDao.delete(spy);
	}

	@Override
	@Transactional(readOnly = false)
	public void update(final Long id, final String name) {
		final Spy spy = spyDao.getById(id);

		if (spy == null) {
			throw new ResourceNotFound("spy", id);
		}
		if (name != null) {
			spy.setName(name);
		}
		spyDao.update(spy);
	}
}
