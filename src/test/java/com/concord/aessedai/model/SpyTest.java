package com.concord.aessedai.model;

import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.hamcrest.CoreMatchers.*;

public class SpyTest {
	
	private static Spy spy;
	
	@BeforeClass
	public static void setup() {
		spy = new Spy();
	}
	
	@Test
    public void learnFrench() {
		spy.learnLanguage(new Language("french", "fr"));
        assertThat(spy.getLanguages().size(), is(1));
    }

}
